﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(int score)
        {
            string level = "";
            if (score < 19 && score >= 0)
            {
                level = "E";
            }
            else if (score < 39 && score >= 20)
            {
                level = "D";
            }
            else if (score < 59 && score >= 40)
            {
                level = "C";
            }
            else if (score < 79 && score >= 60)
            {
                level = "B";
            }
            else if (score <= 100 && score >= 80)
            {
                level = "A";
            }
            ViewBag.score = score;
            ViewBag.level = level;
            return View();
        }
    }
}