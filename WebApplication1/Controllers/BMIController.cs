﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }
        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (data.height < 100 || data.height > 250)
            {
                ViewBag.HeightError = "身高請輸入100-250公分";
            }
            if (data.weight < 30 || data.weight > 200)
            {
                ViewBag.WeightError = "體重請輸入30-200公斤";
            }


            if (ModelState.IsValid)
            {
                float m_height = data.height / 100;
                float BMI = data.weight / (m_height * m_height);
                string level = "";
                if (BMI < 18.5)
                {
                    level = "體重過輕";
                }
                else if (18.5 <= BMI && BMI < 24)
                {
                    level = "正常範圍";
                }
                else if (24 <= BMI && BMI < 27)
                {
                    level = "過重";
                }
                else if (27 <= BMI && BMI < 30)
                {
                    level = "輕度肥胖";
                }
                else if (30 <= BMI && BMI < 35)
                {
                    level = "中度肥胖";
                }
                else if (35 <= BMI)
                {
                    level = "重度肥胖";
                }
                data.BMI = BMI;
                data.level = level;
            }
            return View(data);
        }

    }
}